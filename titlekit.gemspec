require File.expand_path('../lib/titlekit/version', __FILE__)

Gem::Specification.new do |spec|
  spec.author = 'Simon Repp'
  spec.description = 'Featureful Ruby library for SRT / ASS / SSA subtitles'
  spec.homepage = 'https://codeberg.org/simonrepp/titlekit'
  spec.license = 'MIT'
  spec.name = 'titlekit'
  spec.summary = 'Featureful Ruby library for SRT / ASS / SSA subtitles'

  spec.add_runtime_dependency('rchardet19', '~> 1.3.7')
  spec.add_runtime_dependency('treetop', '~> 1.6.11')

  spec.add_development_dependency('rspec', '~> 3.11.0')

  spec.platform = Gem::Platform::RUBY
  spec.required_ruby_version = '>= 2.4.0'
  spec.files = `git ls-files`.split($\)
  spec.executables = spec.files.grep(%r{^bin/}).map { |f| File.basename(f) }
  spec.test_files = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']
  spec.version = Titlekit::VERSION
end
